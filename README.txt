Blade

Description
--------------------------
Enhance and extend current Drupal PHPTemplate with Laravel Blade template.

Requirements
--------------------------
Composer Manager
Laravel-Blade

Installation
--------------------------
1. Copy the entire blade directory the Drupal sites/all/modules
directory or use Drush with drush dl blade.
2. Login as an administrator. Enable the module on the Modules page.
3. Use Composer Manager and Composer to download Laravel-Blader.

Usage
--------------------------
Now you can edit current tpl.php and add more Laravel Blade syntax :
http://laravel.com/docs/templates#blade-templating
